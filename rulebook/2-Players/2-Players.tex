%! TEX root = ../rulebook.tex

\section{Player Creation and Transactions}

\subsection{Roster information}
\begin{deepEnumerate}
	\item Rosters must have a minimum of 9 players including 1 pitcher.	A team may not field a roster of over 18
	players.
	\begin{deepEnumerate}
		\item A pinch-hitting GM (see GMs as Players) does not occupy a roster slot.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Player Creation}
\begin{deepEnumerate}
	\item Player names submitted by a player cannot be changed without approval from the Office of the Commissioners.
	\begin{deepEnumerate}
		\item Names may not match or strongly resemble those of active MLB players.
		\item Names may not bully or demean other players or members of the community.
		\item Names must only contain the following characters: capital letters (A-Z), lowercase letters (a-z), digits
		(0-9), blank space ( ), comma (,), hyphen (-), period (.), plus (+), and single-quote (').
		\item Letters with diacritics may also be used.
		\item A name may not begin or end with a blank space.
		\item Names must begin with a letter or digit and must end with any character except a blank space. A blank
		space cannot follow another blank space.
		\item Names must be 40 characters in length or fewer.
		\item OOTC may reserve the right to reject player names on a case by case basis
		\item Players will have a limit on name changes.
		\begin{deepEnumerate}
			\item Offseason name changes are allowed.
			\item Midseason name changes are limited to one per player.
			\item OOTC reserves the right to make exceptions to this rule.
		\end{deepEnumerate}
		\item No alternate accounts may be used. Players and GMs are limited to using one account in the game. Discovery
		of the use of an alternate account will result in an immediate and permanent ban of the player and all current,
		past, and future accounts used or held by that player.
		\begin{deepEnumerate}
			\item Reporter accounts are the only exception (see \hyperref[sec:reporters]{reporter rules})
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item Players are required to have a Reddit or Discord account in good standing to sign up, as determined by
	Moderators. Generally, a player should have an active Reddit account older than six months or a Discord account
	older than six months. Players who do not have an active account may be rejected as part of the league's policy on
	alternate accounts. Players not meeting these requirements may be vouched for by existing players in good standing,
	subject to Moderator approval.
	\item All players will choose to be a hitter or pitcher, a hand, and a type.
	\begin{deepEnumerate}
		\item Hitters pitching in MLR will be assigned the Position pitching type. Pitchers hitting in MLR will be
		assigned the Pitcher hitting type.
		\item Hitters pitching in MiLR may select a pitching type. Pitchers hitting in MiLR may select a hitting type.
		These types will only be used in MiLR games.
		\item Position players have the opportunity to choose their primary and secondary positions. Available secondary
		positions for each primary position are listed below.
	
		\begin{center}
			\begin{tabular}{|l|l|}
				\hline
				Primary Position & Available Secondary Positions         \\
				\hline
				Catcher          & First Base, Third Base                \\
				\hline
				First Base       & Third Base, Left Field                \\
				\hline
				Second Base      & Shortstop, First Base, Right Field    \\
				\hline
				Third Base       & Second Base, Shortstop, Left Field    \\
				\hline
				Shortstop        & Second Base, Third Base, Center Field \\
				\hline
				Left Field       & Right Field, First Base               \\
				\hline
				Center Field     & Left Field, Right Field               \\
				\hline
				Right Field      & Center Field, First Base              \\
				\hline 
			\end{tabular}
		\end{center}
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Lineup Creation}
\label{sec:lineups}
\begin{deepEnumerate}
	\item GMs are required to send in their lineups prior to the start of the session. Starting lineups must be valid
	when submitted. Invalid starting lineups must be fixed as soon as possible, or risk being overridden by a default
	lineup or one set by League Operations Managers (see \nameref{sec:game rules}). 
	\begin{deepEnumerate}
		\item A valid lineup consists of 9 (or 10 when using a DH) players,	in positions they are each eligible for,
		with all positions represented.
		\begin{deepEnumerate}
			\item Players are eligible to play their primary, secondary, or tertiary positions.
			\item Players playing out of position may be allowed on a case-by-case basis with valid reason,	and must be
			approved by League Operations Managers.
		\end{deepEnumerate}
		\item Pitchers may only play as pitchers. They may not play a defensive position other than pitcher.
		\begin{deepEnumerate}
			\item Pitchers may pinch hit or play a position in case of emergency.
		\end{deepEnumerate}
		\item Any player is eligible to slot in as DH.
		\item Since catcher is not an available secondary position,	a backup "emergency" catcher may be designated prior
		to each game.
		\item Position player pitching
		\begin{deepEnumerate}
			\item Position players may come in to pitch in a game when at least one of the following are true:
			\begin{deepEnumerate}
				\item All pitchers on the team's roster are unavailable.
				\item League Operations Managers believes the substitution of a position player to pitch is necessary or
				justified.
			\end{deepEnumerate}
			\item Position players that pitch will never get a hand bonus and have a special pitching range
			(see \nameref{sec:ranges index}).
		\end{deepEnumerate}
		\item GMs as players
		\label{sec:GMs and Co-GMs as Players}
		\begin{deepEnumerate}
			\item GMs and Co-GMs have two options if they would like to play in a game.
			\begin{deepEnumerate}
				\item Full time player-GM/Co-GM
				\begin{deepEnumerate}
					\item Full time player-GMs/Co-GMs occupy a roster spot on their team and are eligible to start in a
					game for their team.
					\item Player GMs/Co-GMs choose a hitter or pitcher type, positions (if hitter), and hand. Player
					GMs/Co-GMs can change these during the offseason.
				\end{deepEnumerate}
				\item Pinch hitter GM/Co-GM
				\begin{deepEnumerate}
					\item Pinch hitter GMs/Co-GMs do not occupy a roster slot on their team and are not eligible to
					start in a game for their team.
					\item Pinch hitter GMs/Co-GMs choose a hitter type, pitcher type, and hand. Pinch hitter GMs can
					change these during the offseason.
					\item Pinch hitter GMs/Co-GMs may pinch hit for any player at any point in the game. They assume the
					position of the player they are replacing, but cannot be moved to other positions once in the game.
				\end{deepEnumerate}
			\end{deepEnumerate}
		\end{deepEnumerate}
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Signing Free Agents}
\begin{deepEnumerate}
	\item After signing up, players will be posted to the GM channel and the free agent list by Moderators. Once
	posted, they enter a 12-hour no-signing period.
	\begin{deepEnumerate}
		\item Free agents cannot officially accept a contract until the 12-hour window has passed. This time should be
		used to consider offers from every team who extends one.
		\item The time and date of the no-sign period will be included on the free agent listing.
	\end{deepEnumerate}
	\item Free agents signed are not eligible to play for their MLR team during the current MLR session.
	\begin{deepEnumerate}
		\item Free agents gifted by the Office of the Commissioners are exempt from this rule.
		\item Newly signed players are eligible to play in MiLR during the session they are signed.
	\end{deepEnumerate}
	\item To sign a free agent to a team, a GM must post a confirmation from the player	(date included) to the GM
	channel in the Discord.
	\item League Operations Managers reserve the right to deny free agent signings if they do not follow procedure or in
	the case of conflict, such as multiple teams claiming a free agent.
	\item Players that are not listed on the free agent list may not be signed prior to their posting. This includes
	free agents who have been wiped from the list (they will need to sign up and be posted again).
	\item A free agent may inform League Operations Managers they are retiring, in which case they will be removed from
	the list.
	\item Free agents cannot be signed after the start of session 15.
\end{deepEnumerate}

\subsection{Releasing Players/Waivers}
\begin{deepEnumerate}
	\item A player may be released at any time for any reason.
	\item Players will be placed on waivers for 24 hours when released.
	\begin{deepEnumerate}
		\item For the first three sessions of the season, Waiver Claim order is the same as the draft order in the most
		recent draft.
		\item After Session 3 ends, the Waiver Claim order for each player will be determined by applying the
		\hyperref[sec:Draft order]{Draft order} to the league standings as of the last session in which all games have
		been completed.
		\begin{deepEnumerate}
			\item If a session has been completed but games are continuing past the end of the session, 
			that session will be used and the records of those teams once those games have been completed will be used.
		\end{deepEnumerate}
		\item If the player clears waivers, they revert to free agent status and may be signed by any team.
	\end{deepEnumerate}
	\item If a player Auto BB's/K's 3 times in a single season, they are automatically released from their team and
	cannot sign with a team for 3 sessions.
	 \begin{deepEnumerate}
		\item A player cannot sign back with the same team they were released from due to Auto BB's/K's. 
		\item MLR and MiLR are considered separate for this purpose (a player can Auto 3 times in MiLR and not be
		released from their MLR team).
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Placing Players on Hiatus}
\begin{deepEnumerate}
	\item If a player knows they will unavailable for a time, their GM may place them on the hiatus list.
	\begin{deepEnumerate}
		\item Valid hiatus reasons include, but are not limited to,	parental responsibilities, vacations, mental health,
		school reasons, etc.
		\item The Office of the Commissioners must approve all hiatus list additions,	and GMs must provide proof of a
		player's situation to corroborate the request.
		\item Teams must be able to field a valid lineup (see \nameref{sec:lineups}) without the player being moved to
		hiatus.
	\end{deepEnumerate}
	\item Players on the hiatus list do not occupy a roster slot and may not play in any game, but remain under team
	control.
	\begin{deepEnumerate}
		\item Teams must recognize players on hiatus and either activate them to their roster prior to the start of the
		draft, or release them into free agency. See rule 2.14.4 for Free Agency rulings on signing back to the same
		team following the draft.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Retired Players}
\begin{deepEnumerate}
	\item A player may retire any time.
	\begin{deepEnumerate}
		\item Retirement immediately removes the player from the team's active roster.
		\item The retired player must be substituted out of any game they are currently playing.
	\end{deepEnumerate}
	\item If the player returns in the same season in which they retired, the player will be returned to the team they
	left upon retirement.
	\begin{deepEnumerate}
		\item GMs may return the player to the active roster, release the player, or trade the player. This must be done
		prior to the start of the following session. Failure to take action on a returning player will result in
		releasing the player.
		\begin{deepEnumerate}
			\item GMs will have at least 48 hours to take action. If the player returns with fewer than 48 hours
			remaining in the current session, the decision will be due by the end of the following session.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item If a retired player returns after the Paper Cup of the season they retired in, they will be declared a free
	agent.
	\item The rights of retired players who have not returned cannot be traded.
	\item Returning retired players may not change any details about their player (type, hand, position) except to the
	extent otherwise allowed by the rules.
	\item Any player that retires after the start of session 12 cannot unretire after the draft and sign back with the
	same team until the end of session 4 of the following season.
	\begin{deepEnumerate}
		\item If unretiring and signing back with the same team does not benefit the team per roster rules, they may do
		so.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Trading}
\begin{deepEnumerate}
	\item Trades may occur between two or more MLR teams.
	\begin{deepEnumerate}
		\item A trade is finalized when all teams involved in the trade confirm it in the GM channel, subject to OOTC
		approval.
		\item The players involved in a trade are moved to their new teams after the current session, prior to the start
		of the next session.
	\end{deepEnumerate}
	\item Good Faith Trading
	\begin{deepEnumerate}
		\item All GMs must participate in trade discussions in good faith.
			\begin{deepEnumerate}
			\item Good Faith is here defined as not grossly misrepresenting the activity of a player either in their
			level of discord activity or the last time they were active.
			\end{deepEnumerate}
		\item If any GM is found to have misrepresented the conditions of the trade, that trade is void unless
		renegotiated
		\item Any trade that is found to grossly benefit one team may be overturned at the discretion of the OOTC 
	\end{deepEnumerate}
	\item Trade Deadline
	\begin{deepEnumerate}
		\item The trade deadline is set at 12pm ET on the first day of session 13.
		\item Players to be named later and Future Considerations cannot be settled by moving players after the trade
		deadline. You must either settle these before the deadline or during the offseason.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Replacement of General Managers}
\begin{deepEnumerate}
	\item Unplanned GM Departure
	\begin{deepEnumerate}
		\item Unplanned departure here is defined as the GM unexpectedly leaving the GM position for any reason,
		including but not limited to stepping down, retiring, or being removed via mutiny.
		\item League Operations Managers will choose an Interim GM from the active roster.
		\begin{deepEnumerate}
			\item Any Interim GM appointed by an Unplanned Departure is unable to make any roster moves or change player
			appointments without approval from League Operations Managers while the search for a new GM is ongoing.
			\item Any Interim GM appointed by an Unplanned Departure shall be made aware that their appointment is by no
			means permanent and that they will be removed if a different candidate is selected for the Primary GM
			position.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item Planned GM Departure
	\begin{deepEnumerate}
		\item Planned departure here is defined as the GM putting forth a plan and/or informing League Operations Managers
		or Moderators about their intentions to depart from being the General Manager of their team for any reason. 
		\item The departing General Manager shall, if possible, remain the GM of the team until a suitable candidate is
		selected to replace them.
		\begin{deepEnumerate}
			\item If the departing GM is unable to maintain their position until a new candidate is selected, the
			departure shall be classed as unplanned.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item GM Replacement Procedure
	\begin{deepEnumerate}
		\item A team with a departing GM may put forth a candidate(s) to replace them
		\begin{deepEnumerate}
			\item It is expected of the team to determine who to submit as the Internal GM Replacement candidate
			themselves, and then inform League Operations Managers.
			\item If the team cannot collectively come to a decision on who to put forth within 7 days of the original
			GM's departure, the Internal GM Replacement procedure shall be skipped and the GM Replacement Voting
			Procedure shall continue.
			\item An internal GM Replacement candidate must currently be on the team's active roster, have majority
			approval from League Operations Managers, and either:
			\begin{deepEnumerate}
				\item Have League Operations Managers run a vote open to all team members where at least 75\% of
				those who voted are in favor of the candidate.
				\item Be the current Co-GM or Interim GM of the team, with at least 3 sessions or 30 days of being in
				either position AND have League Operations Managers run a vote open to all team members where at
				least 51\% of those who voted are in favor of the candidate.
		\end{deepEnumerate}
		\item If the team cannot collectively come to a decision on who to put forth within 7 days of the original GM's
		departure, the Internal GM Replacement procedure shall be skipped and the GM Replacement Voting Procedure shall
		continue.
	\end{deepEnumerate}
	\item If the team is unable to replace a GM internally, a full team vote with 3 available candidates shall occur.
		\begin{deepEnumerate}
			\item League Operations Managers will select 3 candidates for a team vote.
			\begin{deepEnumerate}
				\item These candidates can be from any team's roster, including the team who is having their GM
				replaced.
				\item League Operations Managers will create and maintain a list of candidates from external sources
				and include up to 3 internal candidates for consideration.
				\begin{deepEnumerate}
					\item This list of candidates will be voted on by League Operations Managers via Ranked Choice
					Voting to establish a ranking. The top 3 candidates will be selected to present to the team for
					consideration.
				\end{deepEnumerate}
				\item League Operations Managers may consult whomever they wish on the replacement of a General
				Manager, but only League Operations Managers shall vote on who becomes a candidate and is submitted
				to the team.
			\end{deepEnumerate}
			\item All currently rostered players on the team are eligible to vote and receive one vote each.
			\begin{deepEnumerate}
				\item If a vote takes place prior to the end of the Paper Cup, all departing players will have a vote.
				\item If a vote takes place after the end of the Paper Cup, no departing players will have a vote. 
			\end{deepEnumerate}
			\item Prior to the vote, there shall be a period of at least 24 hours where any member of the team may ask
			questions to any and all candidates.
			\begin{deepEnumerate}
				\item A  vote can proceed in less than 24 hours only if following all candidates being given a
				reasonable time (no less than 6 hours) to respond to the Q and A the interim GM can show proof all
				members of the team have agreed to proceed to the voting phase.
			\end{deepEnumerate}
			\item All members of the team shall be notified upon the opening of and a couple hours prior to the closing
			of the vote.
			\begin{deepEnumerate}
				\item The vote shall remain open for no less than 24 hours or until 100\% of the players have voted,
				whichever occurs first.
			\end{deepEnumerate}
			\item Once the vote is officially closed, all votes shall be tallied up by League Operations Managers,
			and the candidate with the most votes shall become the new GM.
			\begin{deepEnumerate}
				\item If there is a tie between 2 leading candidates, the candidate with the lowest percentage of votes
				will be removed and a runoff vote will occur between the two tied candidates.
				\item If there is a tie between all three candidates, the voting procedure shall be restarted with 3 new
				candidates submitted to the team.
			\end{deepEnumerate}
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item Discord presence in the main server is required by at least 1 GM, Co-GM, or Interim GM per team. 
	\begin{deepEnumerate}
		\item If the Primary GM has been out of the main server for more than 3 sessions or 30 days within a 180 day
		span, the procedure to replace them shall commence barring any extraordinary situations.
		\begin{deepEnumerate}
			\item Extraordinary situation here is defined as any condition or occurrence that is irregular or out of the
			ordinary that would prevent a GM from performing their duties for an extended period of time greater than
			one month.
			\item GM's must provide sufficient evidence of an extraordinary situation to League Operations Managers in
			order to be exempt.
			\item Any GM removed from their position in this way is ineligible to accept another role as GM, Co-GM,
			Interim GM, or MiLR GM for a period no less than 1 year.
			\item Repeated abuses of this rule shall lead to an investigation and potentially a removal and replacement
			of the GM by League Operations Managers.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item The Mutiny Clause: Additional conditions for GM/Co-GM removal. For mutiny considerations, Auto K/BBs and
	missed lineup submissions are counted separately for each individual GM/Co-GM. 
	\begin{deepEnumerate}
		\item 3 missed lineup submissions by a GM/Co-GM.
		\item 80\% or 10 rostered players vote no confidence for either the GM or Co-GM. 
		\item 2 games lost to Auto-K/BB forfeit or 7 Auto-K/BBs in a single season.
		\begin{deepEnumerate}
			\item At 1 forfeit due to Auto-K/BB or 5 Auto-K/BBs in a single season for a given GM that doesn't have a
			Co-GM, that GM must appoint a Co-GM.
		\end{deepEnumerate}
		\item 75\% of all GMs vote no confidence for a primary GM.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Team Leadership}
\begin{deepEnumerate}
	\item GMs may designate a primary team captain and up to two secondary team captains.
	These players must be disclosed to League Operations Managers and they may be added or removed at any time.
	\begin{deepEnumerate}
		\item Team captains may make emergency substitutions and set emergency lineups in the case of an unavailable GM
		(see \hyperref[sec:substitutions]{Substitutions} and \hyperref[sec:captain lineups]{Captain Lineups}).
		\begin{deepEnumerate}
			\item Umps should not initiate contact with team captains except in extreme cases or otherwise stated in the
			rulebook.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item GM's may designate a Co-GM.
	\begin{deepEnumerate}
		\item Co-GMs take up the Primary Captain spot for a team and have all the same privileges and rights as the GM
		except:
		\begin{deepEnumerate}
			\item Co-GMs may be added or removed at any time by the GM.
			\item GM's may place limitations on the actions Co-GMs can make as they see fit.
			\begin{deepEnumerate}
				\item GM's will be responsible for policing the limitations they place on their Co-GMs.
			\end{deepEnumerate}
			\item League Operations Managers reserve the right to reject any potential Co-GM.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item GM's may designate Interim GM's
	\begin{deepEnumerate}
		\item If a GM knows they will be unable to fulfil their role for a certain amount of time, they may designate a
		player on their active roster to be an Interim GM.
		\begin{deepEnumerate}
			\item Interim GM's are expected to set lineups and to maintain and manage their team while the GM is unable
			to do so.
		\end{deepEnumerate}
	\item Interim GM's may not make roster moves without approval from the GM.
	\item Interim GM's cannot make or change player appointments without approval from the GM.
	\item An Interim GM will be removed from their role once their GM is ready to return or after a date specified by
	the GM.
	\item League Operations Managers reserve the right to reject any potential Interim GM.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Midseason Player Changes}

\begin{deepEnumerate}
	\item Between the start of Session 8 and 24 hours after the finalization of the last scheduled game of Session 8,
	players will be allowed to do the following:
	\begin{deepEnumerate}
		\item Players may opt to change their primary and/or secondary position freely to any other legal position
		combination. 
		\item Players may opt to change their batting type, pitching type, pitching bonus, and/or handedness.  
		\item Players may add or change their third position.
		\begin{deepEnumerate}
			\item This third position does not have the same restrictions as normal secondary positions - that is, any
			position player may choose any position to add as their third position.
		\end{deepEnumerate}
		\item All changes will be processed for Session 9.
	\end{deepEnumerate}
\end{deepEnumerate}

\begin{deepEnumerate}
	\item A GM or Co-GM may change between a PH, Batter or Pitcher GM 1 time per season up through the end of Session
	12. 
	\begin{deepEnumerate}
		\item If a new GM/Co-GM were to take over after Session 12, they are only able to change when they become a
		GM/Co-GM. 
		\begin{deepEnumerate}
			\item If a new GM/Co-GM takes over before Session 12, they are allowed to change when taking over and 1 time
			per season. 
			\item GMs/Co-GMs cannot make a change to their GM style during the playoffs.
			\item If the primary and Co-GM switch roles at some point throughout the season, they do NOT gain an
			additional change. 
		\end{deepEnumerate}
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{Offseason Player Changes}
\label{sec:OPC}
\begin{deepEnumerate}
	\item Each offseason, a player may opt to change their type, handedness, and position freely to any other legal
	position freely. Changes must be declared prior to the offseason declaration form closing. The form will close no
	later than 2 days after the Paper Cup ends.
	\item A player may change positions freely pursuant to the Third position rules.
	\item A pitcher changing positions to become a position player is free to remain with their team or depart their
	team pursuant to the \hyperref[sec:OPM]{Offseason Player Movement rules}.
	\item A GM may change between a PH, Batter or Pitcher GM freely throughout the offseason up until Free Agency opens. 
\end{deepEnumerate}

\subsection{Offseason Player Movement}
\label{sec:OPM}
\begin{deepEnumerate}
	\item At the end of each season, players may opt to depart from the Club to which they are currently signed.
	\begin{deepEnumerate}
		\item Players who signed with their club prior to the Trading Deadline, or who have appeared in at least 6
		regular season MLR or MiLR games, shall be eligible to opt into the free-agent pool. Games played in MiLR as a
		free agent count towards this minimum.
		\item All players opting to depart from their club who do not opt into the free-agent pool shall be part of the
		draft pool.
		\item Players changing position under the \hyperref[sec:OPC]{Offseason Positional Changes rules} remain subject
		to those rules.
	\end{deepEnumerate}
	\item Once the list of players departing clubs at the end of the season has been finalized, General Managers can
	release players still members of their Club.
	\begin{deepEnumerate}
		\item Players released this way shall be subject to the same rules as players who opted to depart from their
		club.
	\end{deepEnumerate}
\end{deepEnumerate}

\subsection{The Draft}
\begin{deepEnumerate}
	\item Number of rounds
	\begin{deepEnumerate}
		\item The Starting number of rounds will be 6.
		\item The number of rounds may be changed depending on the amount of sign-ups.
		\item The Draft Window is defined here as a period beginning from 12am EST/EDT on the day of the draft until a
		moment declared by League Operations Managers following the conclusion of the draft.
	\end{deepEnumerate}
	\item Number of picks
	\begin{deepEnumerate}
		\item Teams may draft up to their roster cap (currently 18 players). Teams may pass at any round.
		If a team passes, they are passing on all remaining rounds. There is no minimum number of players a team must
		draft.
		\item Teams may trade draft picks. A team will be able to claim their own or acquired (via trade) draft picks in
		that round if their player roster is below the current roster limit. A team that has traded their pick for that
		round must also have a player roster below the current roster limit equal to the round the traded pick is from.
		(EX: If a traded pick is from Round 3, then three roster slots must be open from the original team the pick is
		from upon the closing of the draft window. If it is from Round 6, six roster slots must be open).
		\begin{deepEnumerate}
			\item A team who fails to meet this deadline must release players until the team has sufficient empty roster
			slots available by the Draft Window. Players released in this way may choose to either enter free agency or
			enter the draft. A player that chooses to enter Free Agency may not sign with the team they are cut from
			until after Session 3 of that upcoming season.
			\item Draft picks are eligible to be traded during the Draft Window. A team that trades away picks must have
			an empty roster slot available for each pick traded away.
			\begin{deepEnumerate}
				\item Any trade attempted during the Draft Window for picks that are not backed by empty roster slots
				will be vetoed.
				\item Nothing but draft picks can be traded during the Draft Window.
			\end{deepEnumerate}
			\item Supplemental draft picks are not bound by this rule, and can be traded without any roster space
			available by the original trading team.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item Draft order
	\label{sec:Draft order}
	\begin{deepEnumerate}
		\item The Draft Order shall follow the following rules:
		\begin{deepEnumerate}
			\item Teams that did not qualify for the playoffs, starting with the worst record and ending with the best
			\begin{deepEnumerate}
				\item Teams meeting this criteria with the same record will be placed in a group
			\end{deepEnumerate}
			\item Teams losing in their league's Wild Card Series, starting with the worst record and ending with the
			best
			\begin{deepEnumerate}
				\item Teams meeting this criteria with the same record will be placed in a group
			\end{deepEnumerate}
			\item Teams losing in their league's Divisional Series, starting with the worst record and ending with the
			best
			\begin{deepEnumerate}
				\item Teams meeting this criteria with the same record will be placed in a group
			\end{deepEnumerate}
			\item Teams losing in their league's Championship Series, starting with the worst record and ending with the
			best
			\begin{deepEnumerate}
				\item Teams meeting this criteria with the same record will be placed in a group
			\end{deepEnumerate}
			\item MLR Runner-Up
			\item Paper Cup Champion
		\end{deepEnumerate}
		\item Between the First and Second rounds, there shall be a Supplemental Round consisting of, in this order:
		\begin{deepEnumerate}
			\item Winner of the MLR Failures Tournament
			\item GM Compensation Picks
			\begin{deepEnumerate}
				\item GM Compensation Picks are acquired by a team when a player departs to become an MLR GM of another
				team during only the 16 game Regular Season.
				\begin{deepEnumerate}
					\item Teams meeting this criteria with the same record will be placed in a group.
				\end{deepEnumerate}
			\end{deepEnumerate}
		\end{deepEnumerate}
		\item For any ties within any category, the tie shall be broken by strength of schedule, with the lowest
		percentage getting the highest pick.
		\item For any ties remaining after the strength of schedule tiebreaker, apply all of the following:
		\begin{deepEnumerate}
			\item Determine the lowest seeded team among teams in the same division using the
			\hyperref[sec:Division tiebreakers]{Division tiebreakers}
			\item Determine the lowest seeded team among remaining teams in the same league using the
			\hyperref[sec:Wildcard tiebreakers]{Wildcard tiebreakers}
			\item Determine the lowest seeded team among remaining interleague teams using the following:
			\begin{deepEnumerate}
				\item Team losing head-to-head
				\item Lower win percentage against common opponents (minimum of four games)
				\item Lower strength of victory
				\item Worse run differential
				\item Most Autos
				\item Random Number Generator
			\end{deepEnumerate}
		\end{deepEnumerate}
		\item Continue running the tiebreaker from the beginning with the remaining teams until all ties are broken.
		\item In subsequent rounds, for any draft order decided by tiebreaker, the team with the first pick in their
		tiebreaker group in the previous round gets the last pick in the group for the following round, and the rest of
		the teams in the group move up by one draft pick.
		\item If expansion teams are added, they will receive 2 picks at the start of each round in a random, snaking
		order.
	\end{deepEnumerate}
	\item Any free agent may re-sign with their team up until the draft. Any player who is a Free Agent during the draft
	may not re-sign with their old team until the end of session 4. 
	\begin{deepEnumerate}
		\item If a team did not benefit during the draft from said player being off their roster then that player may
		re-sign with their old team under normal free agency rules.
		\item A player may re-sign with their team without penalty, provided there was a change in GM between their
		release and re-signing, and provided their retirement was before the start of session 12.
	\end{deepEnumerate}
\end{deepEnumerate}
